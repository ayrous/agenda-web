package br.senai.sp.informatica.agenda.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.senai.sp.informatica.agenda.model.Contato;

public class ContatoDao {

	// atributos
	Connection connection;

	// construtor
	public ContatoDao() {
		// abre uma conex�o no banco de dados
		this.connection = new ConnectionFactory().getConnection();
	}

	// m�todo salva
	public void salva(Contato contato) {
		// comando sql
		String sql = "INSERT INTO contato" + "(nome, email, endereco, dataNascimento)" + "VALUES(?,?,?,?)";

		try {
			// cria um PreparedStatement com o comando sql
			// java.sql.PreparedStatement
			PreparedStatement stmt = connection.prepareStatement(sql);

			// cria os par�metros para as "?"s
			stmt.setString(1, contato.getNome());
			stmt.setString(2, contato.getEmail());
			stmt.setString(3, contato.getEndereco());

			stmt.setDate(4, new Date(contato.getDataNascimento().getTimeInMillis()));

			// executa a instru��o sql
			stmt.execute();

			// libera o recurso de prepared statement
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// fim do m�todo salva

	// m�todo getLista

	public List<Contato> getLista() {
		try {
		// cria uma lista de contatos
		
		List<Contato> contatos = new ArrayList<>();

		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM contato");

		// guarda os resultados da consulta em um ResultSet(conjunto de resultados)
		ResultSet rs = stmt.executeQuery();

		// enquanto houver um pr�ximo registro no ResultSet...
		while (rs.next()) {

			// cria um novo contato com os dados do ResultSet
			Contato contato = new Contato();
			contato.setId(rs.getLong("id"));
			contato.setNome(rs.getString("nome"));
			contato.setEmail(rs.getString("email"));
			contato.setEndereco(rs.getString("endereco"));

			// trata a dataNascimento
			// cria uma instancia de Calendar
			Calendar data = Calendar.getInstance();

			// define a data do calendar com os dados do ResultSet
			data.setTime(rs.getDate("dataNascimento"));
			contato.setDataNascimento(data);

			// adiciona o contato ao ArryaList contatos
			contatos.add(contato);
		} // fim do while

		// libera o recurso de ResultSet
		rs.close();

		// libera o preparedStatement
		stmt.close();

		// retorna a lista de contatos
		return contatos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}// fim do m�todo getLista
}// fim da classe
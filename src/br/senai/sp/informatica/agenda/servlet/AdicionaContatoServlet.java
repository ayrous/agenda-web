package br.senai.sp.informatica.agenda.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import br.senai.sp.informatica.agenda.dao.ContatoDao;
import br.senai.sp.informatica.agenda.model.Contato;

@WebServlet("/adicionaContato")
public class AdicionaContatoServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// obt�m um PrintWriter
		// esse objeto nos permitir� escrever mensagens no response
		PrintWriter out = res.getWriter();

		// pega os par�metros da requisi��o
		// por meio dos names do form

		String nome = req.getParameter("nome");
		String email = req.getParameter("email");
		String endereco = req.getParameter("endereco");
		String dataNascimento = req.getParameter("dataNascimento");

		// criando o objeto calendario, definindo como null
		Calendar data = null;

		try {
			// converte a data de string para date
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dataNascimento);

			// obt�m uma instancia de Calendar
			data = Calendar.getInstance();

			// convertendo date para Calendar
			data.setTime(date);

		} catch (java.text.ParseException e) {

			// escreve um erro no response
			// (devolve um erro paa o usu�rio)

			out.println("Erro de convers�o de dados!");
			return; // interrompe a execu��o do m�todo service()
		} // fim do try.. catch

		// cria uma nova instancia de contato
		Contato contato = new Contato();

		// define os atributos do contato
		contato.setNome(nome);
		contato.setEmail(email);
		contato.setEndereco(endereco);
		contato.setDataNascimento(data);

		// obtem uma instancia de dao
		ContatoDao dao = new ContatoDao();

		// salva o contato
		dao.salva(contato);

		// devolve uma resposta ao usu�rio
		out.println("<html>");
		out.println("<body>");
		out.println("Contato " + contato.getNome() + " salvo com sucess!");
		out.println("</body");
		out.println("</html>");

	}// fim do m�todo service

}// fim da classe